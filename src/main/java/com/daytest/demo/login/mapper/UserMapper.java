package com.daytest.demo.login.mapper;

import com.daytest.demo.login.model.UserModel;

public interface UserMapper {

    /**
     * 根据用户名，查找用户
     *
     * @param userName
     * @return
     */
    public UserModel selectUserName(String userName);
}
