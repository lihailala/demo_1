package com.daytest.demo.login.controller;

import com.daytest.demo.commons.RestResponse;
import com.daytest.demo.login.service.impl.LoginServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class LoginController {

    static Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    LoginServiceImpl loginService;

    @GetMapping(path = "/login")
    public RestResponse login(@RequestParam String name, @RequestParam String pwd) {

        LOGGER.info("登陆参数 name={}, pwd={}", name, pwd);

        Boolean loginRest = loginService.login(name, pwd);
        Map<String, Object> map = new HashMap(1);
        map.put("login", loginRest);
        return RestResponse.ok(map);
    }

}
