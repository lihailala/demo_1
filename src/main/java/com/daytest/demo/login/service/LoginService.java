package com.daytest.demo.login.service;

public interface LoginService {

    public Boolean login(String name, String password);

}
