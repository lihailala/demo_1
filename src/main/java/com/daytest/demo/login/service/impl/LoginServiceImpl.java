package com.daytest.demo.login.service.impl;

import com.daytest.demo.login.mapper.UserMapper;
import com.daytest.demo.login.model.UserModel;
import com.daytest.demo.login.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    UserMapper userMapper;

    @Override
    public Boolean login(String name, String password) {
        UserModel userModel = userMapper.selectUserName(name);
        if (userModel == null) {
            return false;
        }

        //判断密码是否相等，如果相等就说明密码正确
        if (userModel.getPassword().equals(password)) {
            return true;
        }
        return false;
    }
}
