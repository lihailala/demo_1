package com.daytest.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.daytest.demo.*"})
public class DemoApplication {

    public static void main(String[] arg){
        SpringApplication.run(DemoApplication.class);
    }

}
