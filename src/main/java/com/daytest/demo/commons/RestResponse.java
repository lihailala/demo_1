package com.daytest.demo.commons;

public class RestResponse {

    private int code;

    private Object data;

    public static RestResponse ok(Object data) {
        RestResponse restResponse = new RestResponse();
        restResponse.setCode(ResponseConstants.success);
        restResponse.setData(data);
        return restResponse;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static class ResponseConstants {
        public static int success = 200;
        public static int fail = 201;
    }
}
